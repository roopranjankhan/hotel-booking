package com.HotelBooking.HotelBooking.model;

public enum RoomType {
    SINGLE,
    DOUBLE,
    DELUXE,
    LUXURY
}
