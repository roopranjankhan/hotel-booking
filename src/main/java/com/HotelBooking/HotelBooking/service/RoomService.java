package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Room;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomService {
    public Room findRoom(Long id);
    public String deleteRoom(Long id);
    public Room save(Room room);
}
