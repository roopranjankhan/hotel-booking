package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Customer;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    public Customer findByName(String name);

    public Customer findById(Long id);

    public Customer save(Customer customer);

    public Customer findByEmail(String email);

    public String delete(Long id);

    public Customer updateInfo(Long id, Customer customer);
}
