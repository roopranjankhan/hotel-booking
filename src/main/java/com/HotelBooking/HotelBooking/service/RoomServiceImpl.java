package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Room;
import com.HotelBooking.HotelBooking.repository.RoomRepository;
import org.springframework.stereotype.Service;

@Service
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;

    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Room findRoom(Long id) {
        return roomRepository.findById(id).get();
    }

    @Override
    public String deleteRoom(Long id) {
        roomRepository.deleteById(id);
        return "deleted successfully";
    }

    @Override
    public Room save(Room room) {
        return roomRepository.save(room);
    }
}
