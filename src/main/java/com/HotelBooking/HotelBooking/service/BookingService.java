package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Booking;

public interface BookingService {
    public Booking updateAddress(String address,Long id);
    public Booking findBookingById(Long id);
    public Booking save(Booking booking);
}
