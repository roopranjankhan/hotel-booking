package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Customer;
import com.HotelBooking.HotelBooking.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer findByEmail(String email) {
        return customerRepository.findByEmail(email);
    }

    @Override
    public String delete(Long id) {
        customerRepository.deleteById(id);
        return "delete successfull";
    }

    @Override
    public Customer updateInfo(Long id, Customer customer) {
        Customer customer1 = customerRepository.findById(id).get();
        customer1.setName(customer.getName());
        customer1.setAge(customer.getAge());
        customer1.setPhone(customer.getPhone());
        customer1.setEmail(customer.getEmail());
        return customerRepository.save(customer1);
    }


    @Override
    public Customer findByName(String name) {
        return customerRepository.findByName(name);
    }

    @Override
    public Customer findById(Long id) {
        return customerRepository.findById(id).get();
    }

}
