package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Booking;
import com.HotelBooking.HotelBooking.repository.BookingRepository;
import org.springframework.stereotype.Service;

@Service
public class BookingServiceImpl implements BookingService {
    private final BookingRepository bookingRepository;

    public BookingServiceImpl(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public Booking updateAddress(String address, Long id) {
        if (bookingRepository.findById(id).isPresent()) {
            Booking booking = bookingRepository.findById(id).get();
            booking.setAddress(address);
            return bookingRepository.save(booking);
        }
        return null;
    }

    @Override
    public Booking findBookingById(Long id) {
        return bookingRepository.findById(id).get();
    }

    @Override
    public Booking save(Booking booking) {
        return bookingRepository.save(booking);
    }
}
