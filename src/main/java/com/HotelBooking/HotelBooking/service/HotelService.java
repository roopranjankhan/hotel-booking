package com.HotelBooking.HotelBooking.service;

import com.HotelBooking.HotelBooking.model.Hotel;
import org.springframework.stereotype.Service;

@Service
public interface HotelService {
    public Hotel findHotel(Long id);
    public Hotel save(Hotel hotel);
}
