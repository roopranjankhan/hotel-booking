package com.HotelBooking.HotelBooking;

import com.HotelBooking.HotelBooking.model.*;
import com.HotelBooking.HotelBooking.repository.BookingRepository;
import com.HotelBooking.HotelBooking.service.BookingService;
import com.HotelBooking.HotelBooking.service.CustomerService;
import com.HotelBooking.HotelBooking.service.HotelService;
import com.HotelBooking.HotelBooking.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;

@SpringBootApplication
@EnableSwagger2
public class HotelBookingApplication implements CommandLineRunner {

	private final HotelService hotelService;
	private final RoomService roomService;
	private final CustomerService customerService;
	private final BookingService bookingService;

	private final BookingRepository bookingRepository;

	@Autowired
	public HotelBookingApplication(HotelService hotelService, RoomService roomService, CustomerService customerService, BookingService bookingService, BookingRepository bookingRepository) {
		this.hotelService = hotelService;
		this.roomService = roomService;
		this.customerService = customerService;
		this.bookingService = bookingService;
		this.bookingRepository = bookingRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(HotelBookingApplication.class, args);
	}

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.HotelBooking.HotelBooking")).build();
	}


	@Override
	public void run(String... args) throws Exception {
		Hotel hotel = new Hotel();
		hotel.setAddress("E.Vilde tee");
		hotel.setCity("Tallinn");
		hotel.setEmail("hotel@example.com");
		hotel.setStars(3);
		hotel.setHasGym(true);
		hotel.setCountry("Estonia");
		hotel.setName("Hotel");
		Room room = new Room();
		room.setType(RoomType.DELUXE);
		room.setCapacity(3);
		room.setHotel(hotel);
		hotel.getRooms().add(room);
		Customer customer = new Customer();
		customer.setName("Roop");
		customer.setPhone("7777855432");
		customer.setEmail("roop@ex.com");
		customer.setAge(21);
		Booking booking = new Booking();
		booking.setCustomer(customer);
		booking.setHotel(hotel);
		booking.setRoom(room);
		booking.setAddress("Tallinn,Estonia");
		booking.setStart_date(new Timestamp(23 / 9 / 2010));
		booking.setEnd_date(new Timestamp(25 / 9 / 2010));
		//hotelRepository.save(myHotel);

//        hotelService.findHotel(1l);
//        hotelService.save(hotel);
//        roomService.findRoom(2l);
		System.out.println(hotelService.save(hotel));
		System.out.println(hotelService.findHotel(1l));
		System.out.println(customerService.save(customer));
		System.out.println(roomService.save(room));
		System.out.println(bookingService.save(booking));
		//System.out.println(customerService.findByName("Roop"));

	}

}
