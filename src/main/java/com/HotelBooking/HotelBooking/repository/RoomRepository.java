package com.HotelBooking.HotelBooking.repository;

import com.HotelBooking.HotelBooking.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {
}
