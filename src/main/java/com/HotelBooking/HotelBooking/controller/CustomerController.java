package com.HotelBooking.HotelBooking.controller;

import com.HotelBooking.HotelBooking.exceptions.CustomerNotFoundException;
import com.HotelBooking.HotelBooking.model.Customer;
import com.HotelBooking.HotelBooking.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(value = "/customer/name/{name}")
    public ResponseEntity<Customer> findCustomerByName(@PathVariable("name") String name) {

        Customer customer = customerService.findByName(name);

        if (customer != null)
            return new ResponseEntity(customer, HttpStatus.OK);
        else
            return new ResponseEntity("No name found" + name, HttpStatus.NOT_FOUND);

    }

    @GetMapping(value = "/customer/{id}")
    public ResponseEntity<Customer> findById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(customerService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("No id found" + id, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/customer/delete/{id}")
    public ResponseEntity<Customer> delete(@PathVariable("id") Long id) throws CustomerNotFoundException {
        if (customerService.findById(id) == null) {
            throw new CustomerNotFoundException("customer with id" + id + "not found");
        }
        return new ResponseEntity(customerService.delete(id), HttpStatus.OK);
    }

    @PostMapping(value = "/customer")
    public ResponseEntity<Customer> save(@RequestBody Customer customer) {
        if (customerService.findByEmail(customer.getEmail()) == null) {
            Customer saveCustomer = customerService.save(customer);

            if (saveCustomer.getId() == null)
                return new ResponseEntity("customer already exists", HttpStatus.CONFLICT);
            else
                return new ResponseEntity("customer not present", HttpStatus.CREATED);
        } else
            return new ResponseEntity("email already exists", HttpStatus.CONFLICT);
    }

    @GetMapping(value = "/customer/email/{email}")
    public ResponseEntity<Customer> findByEmail(@PathVariable("email") String email) {

        Customer customer = customerService.findByEmail(email);

        if (customer != null)
            return new ResponseEntity(customer, HttpStatus.OK);
        else
            return new ResponseEntity("No name found" + email, HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/customer/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable("id") Long id, @RequestBody Customer customer) {
        Customer customer1 = customerService.updateInfo(id,customer);
        return new ResponseEntity(customer1, HttpStatus.ACCEPTED);
    }
}
