package com.HotelBooking.HotelBooking.controller;

import com.HotelBooking.HotelBooking.exceptions.HotelNotFoundException;
import com.HotelBooking.HotelBooking.model.Hotel;
import com.HotelBooking.HotelBooking.model.Room;
import com.HotelBooking.HotelBooking.service.HotelService;
import com.HotelBooking.HotelBooking.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoomController {
    private final RoomService roomService;
    private final HotelService hotelService;

    @Autowired
    public RoomController(RoomService roomService, HotelService hotelService) {
        this.roomService = roomService;
        this.hotelService = hotelService;
    }

    @GetMapping(value = "/room/{id}")
    public Room findRoomById(@PathVariable("id") Long id) {
        return roomService.findRoom(id);
    }

    @DeleteMapping(value = "/hotel/{hId}/room/{rId}")
    public ResponseEntity<Hotel> delete(@PathVariable("hId") Long hId, @PathVariable("rId") Long rId) throws HotelNotFoundException {
        if (hotelService.findHotel(hId) == null) {
            throw new HotelNotFoundException("hotel with id" + hId + "not found");
        }
        Hotel hotel = hotelService.findHotel(hId);
        for (Room room : hotel.getRooms()) {
            if (room.getId().equals(rId)) {
                roomService.deleteRoom(rId);
            }
        }
        return new ResponseEntity("room with id: " + rId + " deleted", HttpStatus.OK);
    }

    @PostMapping(value = "/room")
    public ResponseEntity<Room> save(@RequestBody Room room) {
        roomService.save(room);
        return new ResponseEntity("Room created", HttpStatus.CREATED);
    }
}
