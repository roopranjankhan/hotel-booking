package com.HotelBooking.HotelBooking.controller;

import com.HotelBooking.HotelBooking.model.Booking;
import com.HotelBooking.HotelBooking.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookingController {
    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PatchMapping(value = "/booking/{id}/address")
    public ResponseEntity<Booking> updateAddress(@PathVariable("id") Long id, @RequestParam("add") String address) {
        Booking booking = bookingService.updateAddress(address, id);
        return new ResponseEntity(booking, HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping(value = "/booking/{id}")
    public ResponseEntity<Booking> findById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(bookingService.findBookingById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("No id found" + id, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/booking")
    public ResponseEntity<Booking> save(@RequestBody Booking booking) {
        bookingService.save(booking);
        return new ResponseEntity("Booking saved", HttpStatus.CREATED);
    }

}
