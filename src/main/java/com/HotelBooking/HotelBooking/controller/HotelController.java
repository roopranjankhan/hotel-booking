package com.HotelBooking.HotelBooking.controller;

import com.HotelBooking.HotelBooking.model.Hotel;
import com.HotelBooking.HotelBooking.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelController {
    private final HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @GetMapping(value = "/hotel/{id}")
    public Hotel findHotelById(@PathVariable("id") Long id) {
        return hotelService.findHotel(id);
    }
}
