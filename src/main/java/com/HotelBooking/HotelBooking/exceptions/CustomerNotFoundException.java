package com.HotelBooking.HotelBooking.exceptions;

public class CustomerNotFoundException extends Throwable {
    public CustomerNotFoundException(String msg){
        System.out.println(msg);
    }
}
