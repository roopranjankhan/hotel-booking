FROM openjdk:8
ADD target/hotelbooking.jar hotelbooking.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "hotelbooking.jar"]
